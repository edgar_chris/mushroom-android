package ke.co.edgar.mushroom.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import ke.co.edgar.mushroom.R;

public class ShopActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private ProgressDialog pDialog;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fakeIt();
                    return true;
                case R.id.navigation_dashboard:
                    fakeIt();
                    return true;
                case R.id.navigation_notifications:
                    fakeIt();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(true);
        fakeIt();
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void fakeIt(){
        pDialog.setMessage(" Fetching Data ...");
        showDialog();
        pDialog.setMessage(" Fetching Data ...");
        showDialog();
        Toast.makeText(getApplicationContext(), "No items Found in shop Please add some", Toast.LENGTH_LONG).show();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
