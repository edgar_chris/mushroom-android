package ke.co.edgar.mushroom.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import ke.co.edgar.mushroom.App.AppConfig;
import ke.co.edgar.mushroom.App.AppController;
import ke.co.edgar.mushroom.R;


public class SingleDestinationActivity extends AppCompatActivity {

    int destinationId;
    private ProgressDialog pDialog;
    private ImageView featuredImage;
    private TextView name;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_destination);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // collapsing bar
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            destinationId = extras.getInt("destinationID");
            Log.e("ID_D", "hello" + destinationId);
        }
        pDialog.setMessage("Fetching Destination");
        showDialog();
        featuredImage = (ImageView) findViewById(R.id.backdrop);
        description = (TextView) findViewById(R.id.description);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_HOME_TRAVEL_DESTINATION + destinationId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        //  Now store the user in SQLite
                        JSONObject destination = jObj.getJSONObject("destination");
                        String image = destination.getString("image");
                        String name = destination.getString("name");
                        String desc = destination.getString("description");

                        description.setText(desc);
                        collapsingToolbar.setTitle(name);

                        try {
                            Glide.with(SingleDestinationActivity.this)
                                    .load(image)
                                    .into(featuredImage);

                            featuredImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View views) {
                                    Log.e("DI", "2 this->" + destinationId);
                                    Intent intent = new Intent(SingleDestinationActivity.this, SingleDestinationActivity.class);

                                    intent.putExtra("destinationID", destinationId);
                                    startActivity(intent);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(SingleDestinationActivity.this,
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(SingleDestinationActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Single_Destination", "Fetching Error: " + error.getMessage());
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "HOME_DATA_REQUEST");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
