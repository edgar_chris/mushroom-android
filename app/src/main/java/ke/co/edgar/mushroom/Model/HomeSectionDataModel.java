package ke.co.edgar.mushroom.Model;

import java.util.ArrayList;

public class HomeSectionDataModel {


    private String headerTitle;
    private ArrayList<HomeSingleItemModel> allItemsInSection;
    private String profileImg;
    private String username;


    public HomeSectionDataModel() {

    }

    public HomeSectionDataModel(String headerTitle, String username, String profileImg, ArrayList<HomeSingleItemModel> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
        this.username = username;
        this.profileImg = profileImg;
    }


    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }


    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }


    public ArrayList<HomeSingleItemModel> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<HomeSingleItemModel> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}
