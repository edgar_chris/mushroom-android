package ke.co.edgar.mushroom.App;

public class AppConfig {
    // Server user login url
    public static String BASE_URL = "http://mushroom.tinker.co.ke/api/";
    public static String URL_LOGIN = BASE_URL + "login";
    // Server user register url
    public static String URL_REGISTER = BASE_URL + "register";
    // server routes  Travel
    public static String URL_TRAVEL_LIST = BASE_URL + "travel/list";
    public static String URL_SINGLE_TRAVEL_LIST = BASE_URL + "travel/list/item";
    public static String URL_TRAVEL_LIST_NEW = BASE_URL + "travel/new";
    public static String URL_SINGLE_TRAVEL_LIST_EDIT = BASE_URL + "travel/list/edit";
    public static String URL_HOME_TRAVEL_LIST = BASE_URL + "travelList";
    public static String URL_HOME_TRAVEL_FEATURED = BASE_URL + "getFeatured";
    public static String URL_HOME_TRAVEL_DESTINATION = BASE_URL + "destination/";

    public static String URL_SINGLE_HOME_ITEM = "http://androidblog.esy.es/ImageJsonData.php";


    // server routes Gigs

    public static String URL_GIG_LIST = BASE_URL + "gig/list";
    public static String URL_SINGLE_GIG = BASE_URL + "gig/single";
    public static String URL_GIG_NEW = BASE_URL + "gig/new";
    public static String URL_GIG_EDIT = BASE_URL + "gig/edit";


    // server routes TRIPS

    public static String URL_TRIP_LIST = BASE_URL + "trip/list";
    public static String URL_SINGLE_TRIP = BASE_URL + "trip/single";
    public static String URL_TRIP_NEW = BASE_URL + "trip/new";
    public static String URL_TRIP_EDIT = BASE_URL + "trip/edit";

    // server routes for Funding  TRIPS

    public static String URL_TRIP_FUND = BASE_URL + "trip/fund/list";
    public static String URL_SINGLE__FUND = BASE_URL + "trip/fund/single";
    public static String URL_TRIP__FUND_NEW = BASE_URL + "trip/fund/new";
    public static String URL_TRIP__FUND_PAYMENT = BASE_URL + "trip/fund/checkout";


    // server routes  PROFILE

    public static String URL_PROFILE = BASE_URL + "profile";
    public static String URL_PROFILE_EDIT = BASE_URL + "profile/edit";

    //server routes UPDATES
    public static String URL_PROFILE_EMAIL = BASE_URL + "profile/update/email";
    public static String URL_PROFILE_PASSWORD = BASE_URL + "profile/update/password";

}
