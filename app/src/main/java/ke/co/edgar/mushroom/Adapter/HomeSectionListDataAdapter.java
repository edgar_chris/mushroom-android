package ke.co.edgar.mushroom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import ke.co.edgar.mushroom.Activity.HomeSingleItemActivity;
import ke.co.edgar.mushroom.Activity.SingleDestinationActivity;
import ke.co.edgar.mushroom.R;
import ke.co.edgar.mushroom.Model.HomeSingleItemModel;

import java.util.ArrayList;

/**
 * this adapter handles the   home section recyler items
 */

public class HomeSectionListDataAdapter extends RecyclerView.Adapter<HomeSectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<HomeSingleItemModel> itemsList;
    private Context mContext;

    public HomeSectionListDataAdapter(Context context, ArrayList<HomeSingleItemModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_home_list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        HomeSingleItemModel singleItem = itemsList.get(i);
        holder.tvTitle.setText(singleItem.getName());
        holder.itemId.setText(singleItem.getId());
        Glide.with(mContext).load(singleItem.getUrl())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.itemImage);

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected ImageView itemImage;
        protected TextView itemId;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.itemId = (TextView) view.findViewById(R.id.item_id);
            itemId.setVisibility(View.INVISIBLE);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CharSequence destinationId = itemId.getText();
                    Log.e("char", "-" + destinationId);
                    Intent intent = new Intent(v.getContext(), SingleDestinationActivity.class);
                    int number = Integer.parseInt(destinationId.toString());
                    intent.putExtra("destinationID", number);
                    v.getContext().startActivity(intent);
                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }

}