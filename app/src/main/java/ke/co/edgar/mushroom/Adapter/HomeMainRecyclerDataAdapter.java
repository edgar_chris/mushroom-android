package ke.co.edgar.mushroom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import ke.co.edgar.mushroom.Activity.HomeSingleItemActivity;
import ke.co.edgar.mushroom.Activity.MainActivity;
import ke.co.edgar.mushroom.Activity.PrivacyPolicyActivity;
import ke.co.edgar.mushroom.Helper.CircleTransform;
import ke.co.edgar.mushroom.R;
import ke.co.edgar.mushroom.Model.HomeSectionDataModel;

import java.util.ArrayList;

public class HomeMainRecyclerDataAdapter extends RecyclerView.Adapter<HomeMainRecyclerDataAdapter.ItemRowHolder> {

    private ArrayList<HomeSectionDataModel> dataList;
    private Context mContext;

    public HomeMainRecyclerDataAdapter(Context context, ArrayList<HomeSectionDataModel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_home_list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = dataList.get(i).getHeaderTitle();
        final String username = dataList.get(i).getUserName();
        final String profileImg = dataList.get(i).getProfileImg();

        ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection();

        itemRowHolder.itemTitle.setText(sectionName);
        itemRowHolder.username.setText(username);
        // Loading profile image
        Glide.with(mContext).load(profileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(mContext))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemRowHolder.profilePic);

        HomeSectionListDataAdapter itemListDataAdapter = new HomeSectionListDataAdapter(mContext, singleSectionItems);

        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);


        itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);


       /*  itemRowHolder.recycler_view_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        //Allow ScrollView to intercept touch events once again.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                // Handle RecyclerView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });*/

        itemRowHolder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mContext.startActivity(new Intent(mContext, HomeSingleItemActivity.class));
                v.getContext().startActivity(new Intent(v.getContext(), HomeSingleItemActivity.class));


                Toast.makeText(v.getContext(), "click event on more, " + sectionName, Toast.LENGTH_SHORT).show();


            }
        });







       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;
        protected TextView username;
        protected ImageView profilePic;

        protected RecyclerView recycler_view_list;

        //protected Button btnMore;


        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.username = (TextView) view.findViewById(R.id.username);
            this.profilePic = (ImageView) view.findViewById(R.id.profilePic);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            // this.btnMore = (Button) view.findViewById(R.id.btnMore);


        }

    }

}