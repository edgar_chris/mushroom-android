package ke.co.edgar.mushroom.Fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.edgar.mushroom.Activity.HomeSingleItemActivity;
import ke.co.edgar.mushroom.Activity.LoginActivity;
import ke.co.edgar.mushroom.Activity.MainActivity;
import ke.co.edgar.mushroom.Activity.SingleDestinationActivity;
import ke.co.edgar.mushroom.Adapter.HomeMainRecyclerDataAdapter;
import ke.co.edgar.mushroom.Adapter.HomeSingleItemDataAdapter;
import ke.co.edgar.mushroom.App.AppConfig;
import ke.co.edgar.mushroom.App.AppController;
import ke.co.edgar.mushroom.Model.Album;
import ke.co.edgar.mushroom.Adapter.AlbumsAdapter;
import ke.co.edgar.mushroom.Model.HomeSectionDataModel;
import ke.co.edgar.mushroom.Model.HomeSingleItemModel;
import ke.co.edgar.mushroom.R;

public class HomeFragment extends Fragment {


    private RecyclerView recyclerView;
    private RecyclerView homeMainRecycler;
    private AlbumsAdapter adapter;
    private List<Album> albumList;
    ArrayList<HomeSectionDataModel> homeData;
    private JSONObject mushroomData;
    private ImageView featuredImage;
    private TextView title;
    private TextView caption;
    int destinationId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        // collapsing bar
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    // collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });

        featuredImage = (ImageView) view.findViewById(R.id.backdrop);
        title = (TextView) view.findViewById(R.id.love_music);
        caption = (TextView) view.findViewById(R.id.caption);
        getMushroomDFeatured(new VolleyFeaturedCallback() {
            @Override
            public void onSuccess(JSONObject featured) {
                try {
                    String titleTx = featured.getString("title");
                    String captionTx = featured.getString("caption");
                    String image = featured.getString("image");
                    final int destinationId = featured.getInt("destination");
                    title.setText(titleTx);
                    caption.setText(captionTx);
                    Log.e("DI", "1 this->" + destinationId);
                    try {
                        Glide.with(getActivity())
                                .load(image)
                                .into(featuredImage);

                        featuredImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View views) {
                                Log.e("DI", "2 this->" + destinationId);
                                Intent intent = new Intent(getActivity(), SingleDestinationActivity.class);

                                intent.putExtra("destinationID", destinationId);
                                startActivity(intent);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        });

        homeData = new ArrayList<HomeSectionDataModel>();
        homeMainRecycler = (RecyclerView) view.findViewById(R.id.homeMainRecycler);
        getMushroomData(new VolleyCallback() {
            @Override
            public void onSuccess(ArrayList<HomeSectionDataModel> homeData) {
                homeMainRecycler.setHasFixedSize(true);
                homeMainRecycler.addItemDecoration(new GridSpacingItemDecoration(4, dpToPx(10), true));
                homeMainRecycler.setItemAnimator(new DefaultItemAnimator());
                HomeMainRecyclerDataAdapter adapter = new HomeMainRecyclerDataAdapter(getActivity(), homeData);
                homeMainRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                homeMainRecycler.setAdapter(adapter);
            }
        });


        return view;
    }


    private void getMushroomDFeatured(final VolleyFeaturedCallback callback) {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_HOME_TRAVEL_FEATURED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        //  Now store the user in SQLite
                        JSONObject featured = jObj.getJSONObject("featured");
                        callback.onSuccess(featured);
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity().getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("HOME_FEATURED", "Fetching Error: " + error.getMessage());
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "HOME_DATA_REQUEST");
        //return mushroomData;


    }


    private void getMushroomData(final VolleyCallback callback) {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_HOME_TRAVEL_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        //  Now store the user in SQLite
                        JSONObject travelist = jObj.getJSONObject("travelist");
                        JSONArray collection = travelist.getJSONArray("collection");
                        for (int i = 0; i < collection.length(); i++) {
                            HomeSectionDataModel dm = new HomeSectionDataModel();
                            JSONObject collectionJson = null;
                            try {
                                collectionJson = collection.getJSONObject(i);
                                String collectionName = collectionJson.getString("collection_name");
                                String collectionImage = collectionJson.getString("collection_guide_image");
                                String collectionUser = collectionJson.getString("collection_guide_name");
                                int collectionId = collectionJson.getInt("collection_id");
                                dm.setHeaderTitle(collectionName);
                                dm.setProfileImg(collectionImage);
                                dm.setUserName(collectionUser);

                                JSONArray destination = collectionJson.getJSONArray("destination");
                                // create list
                                ArrayList<HomeSingleItemModel> singleItem = new ArrayList<HomeSingleItemModel>();
                                for (int d = 0; d < destination.length(); d++) {
                                    JSONObject destinationJson = null;
                                    try {
                                        destinationJson = destination.getJSONObject(d);
                                        String destinationName = destinationJson.getString("destination_name");
                                        String destinationImage = destinationJson.getString("destination_image");
                                        String destinationDesc = destinationJson.getString("destination_desc");
                                        String destinationId = destinationJson.getString("destination_id");
                                        singleItem.add(new HomeSingleItemModel(destinationName, destinationImage, destinationId));
                                    } catch (JSONException e) {

                                        e.printStackTrace();

                                    }
                                }

                                dm.setAllItemsInSection(singleItem);
                                homeData.add(dm);

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }

                        }
                        callback.onSuccess(homeData);
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getActivity().getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("HOME_DATA", "Fetching Error: " + error.getMessage());
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, "HOME_DATA_REQUEST");
        //return mushroomData;


    }

    public void getHomeData() {

        // loop data
        for (int i = 1; i <= 5; i++) {

            HomeSectionDataModel dm = new HomeSectionDataModel();


            dm.setHeaderTitle("Best Travel 201" + (i + 6));
            dm.setProfileImg("https://lh4.googleusercontent.com/-basKbsXHhx4/AAAAAAAAAAI/AAAAAAAAAAw/SANntMQpv5k/photo.jpg");
            dm.setUserName("Lonely Planet " + i);

            // create list
            ArrayList<HomeSingleItemModel> singleItem = new ArrayList<HomeSingleItemModel>();
            for (int j = 0; j <= 5; j++) {


                singleItem.add(new HomeSingleItemModel("Destination " + j, "http://lorempixel.com/output/nature-h-c-250-350-3.jpg", "2"));
            }

            dm.setAllItemsInSection(singleItem);

            homeData.add(dm);

        }
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public interface VolleyCallback {
        void onSuccess(ArrayList<HomeSectionDataModel> homeData);
    }


    public interface VolleyFeaturedCallback {
        void onSuccess(JSONObject featured);
    }
}
