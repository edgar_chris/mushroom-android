package ke.co.edgar.mushroom.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.edgar.mushroom.Adapter.HomeSingleItemDataAdapter;
import ke.co.edgar.mushroom.Adapter.HomeSingleItemRecyclerViewAdapter;
import ke.co.edgar.mushroom.App.AppConfig;
import ke.co.edgar.mushroom.R;

public class HomeSingleItemActivity extends AppCompatActivity {
    List<HomeSingleItemDataAdapter> homeSingleListData;

    RecyclerView recyclerView;
    String HTTP_JSON_URL = AppConfig.URL_SINGLE_HOME_ITEM;
    String TITLE = "image_title";
    String IMG_SRC = "image_url";
    String IMG_DESC = "image_desc";
    JsonArrayRequest SingleItemRequest;
    RequestQueue requestQueue;
    View view;
    int RecyclerViewItemPosition;

    RecyclerView.LayoutManager layoutManagerOfrecyclerView;

    RecyclerView.Adapter recyclerViewadapter;

    ArrayList<String> homeSingleItemArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_single_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(" ");
        homeSingleItemArray = new ArrayList<>();
        homeSingleListData = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.home_single_item_recycler);
        recyclerView.setHasFixedSize(true);
        layoutManagerOfrecyclerView = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManagerOfrecyclerView);
        getSingleHomeItem();


        // collapsing bar
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.single_home_item_collapsing_layout);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.single_home_item_appbar);
        appBarLayout.setExpanded(true);
        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    //collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });

        // Implementing Click Listener on RecyclerView.
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(HomeSingleItemActivity.this, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent motionEvent) {

                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

                view = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (view != null && gestureDetector.onTouchEvent(motionEvent)) {

                    //Getting RecyclerView Clicked Item value.
                    RecyclerViewItemPosition = Recyclerview.getChildAdapterPosition(view);

                    // Showing RecyclerView Clicked Item value using Toast.
                    Toast.makeText(HomeSingleItemActivity.this, homeSingleItemArray.get(RecyclerViewItemPosition), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(HomeSingleItemActivity.this, SingleDestinationActivity.class);
                    startActivity(intent);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void getSingleHomeItem() {

        SingleItemRequest = new JsonArrayRequest(HTTP_JSON_URL,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        ParseJSonResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(HomeSingleItemActivity.this);

        requestQueue.add(SingleItemRequest);
    }

    public void ParseJSonResponse(JSONArray array) {

        for (int i = 0; i < array.length(); i++) {
            HomeSingleItemDataAdapter singleHomeItem = new HomeSingleItemDataAdapter();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);
                singleHomeItem.setImageTitle(json.getString(TITLE));
                // dding image title name in array to display on RecyclerView click event.
                homeSingleItemArray.add(json.getString(TITLE));
                singleHomeItem.setImageUrl(json.getString(IMG_SRC));

            } catch (JSONException e) {

                e.printStackTrace();
            }
            homeSingleListData.add(singleHomeItem);
        }
        recyclerViewadapter = new HomeSingleItemRecyclerViewAdapter(homeSingleListData, this);

        recyclerView.setAdapter(recyclerViewadapter);
    }

}
